import numpy as np
import copy
from sys import argv
from random import sample
from random import shuffle
from scipy.spatial import distance
import matplotlib.pyplot as plt

'''
Constants
'''
CONST_CROSS_RATE = 98
CONST_MUTATION_RATE = 4
CONST_POPULATION_SIZE = 100
CONS_GENERATIONS = 10000
global CONST_CITIES
CONST_CITIES = 50
CONST_LIMIT = 20
global CITIES
CITIES = [sample(range((CONST_LIMIT)*-1,(CONST_LIMIT)),2) for x in range(CONST_CITIES)]

class Cromossomo:
	def __init__(self, cr=[]):
		# verify if is null, if is null make randon
		self.cro = self.make_initial_cro() if cr == [] else cr
		self.fit = self.calc_fitnes()
	def make_initial_cro(self):
		city = copy.copy(CITIES)
		shuffle(city)
		return city
	def calc_fitnes(self):
		dist = 0 
		for i in range(CONST_CITIES-1):
			dist += distance.euclidean(self.cro[i],self.cro[i+1])
		dist += distance.euclidean(self.cro[CONST_CITIES-1],self.cro[0])
		return dist # calcula o fitnes

def show_population(pop):
	pop = sorted(pop,key = lambda x:x.fit)
	print('\n')
	for i in range(len(pop)):
		print('Ind: '+str(i)+'\t\tCro: '+str(pop[i].cro)+'\t\t\tFit: '+str(pop[i].fit))

def make_tournament(pop):
	pop_middle = []
	best = get_best(pop)
	for i in range(len(pop)):
		c1,c2=np.random.choice((len(pop)),2,replace=False) #select 2 ind to make tournament
		cr_max = min([pop[c1],pop[c2]],key = lambda x:x.fit) #verify what is the best
		pop_middle.append(Cromossomo(cr = cr_max.cro))
	pop_middle[pop_middle.index(get_worst(pop_middle))] = copy.deepcopy(best)
	return pop_middle

def make_obx(pop):
	pop_middle = []
	best = get_best(pop)
	for i in range(int(len(pop)/2)):
		#seleciona dois cromossomos da populacao para fazer o cross-over
		c1,c2 = np.random.choice((len(pop)),2, replace=False)
		#sorteia a problabilidade de fazer o cross-over
		prob = np.random.randint(0,101) 

		# print(prob)
		#verifica se a probabilidade sortiada caiu para fazer o cross-over
		if(prob<=CONST_CROSS_RATE): # if prob < cros rate do the cross
			#Pontos de ordem para o obx
			# cut = sample(range(1, CONST_CITIES-1),int(CONST_CITIES/2))
			cut = sample(range(1, CONST_CITIES-1),int(CONST_CITIES/2))
			f1 = []
			f2 = []

			ord1 = find_ord(cut,pop[c1].cro,pop[c2].cro)
			ord2 = find_ord(cut,pop[c2].cro,pop[c1].cro)
			cont = 0
			# print(cut)	
			# print(pop[c1].cro,' ',pop[c2].cro)
			
			for j in range(CONST_CITIES):
				if(j in cut):	
					f1.append(pop[c2].cro[ord1[cont]])
					f2.append(pop[c1].cro[ord2[cont]])
					cont = cont + 1
				else:
					f1.append(pop[c1].cro[j])
					f2.append(pop[c2].cro[j])

			# print(f1,' ',f2)
			pop_middle.append(Cromossomo (cr = f1))
			pop_middle.append(Cromossomo (cr = f2))
		else:
			pop_middle.append(Cromossomo(cr = pop[c1].cro))
			pop_middle.append(Cromossomo(cr = pop[c2].cro))
	pop_middle[pop_middle.index(get_worst(pop_middle))] = copy.deepcopy(best)
	return pop_middle

def find_ord(cut, p1, p2):
	# print(cut,p1,p2)
	aux = []
	for i in range(len(cut)):
		aux.append(p2.index(p1[cut[i]]))	
	aux.sort()
	return aux

def make_ord_element_mutation(pop):	
	best = min(pop,key = lambda x:x.fit)
	for i in range(len(pop)):
		# Sorteia probabilidade de fazer a mutacao
		prob = np.random.randint(0,101) 

		if(prob < CONST_MUTATION_RATE):
			c1,c2 = np.random.choice(range(0,CONST_CITIES),2, replace=False)
			aux = pop[i].cro[c1]
			pop[i].cro[c1] = pop[i].cro[c2]
			pop[i].cro[c2] = aux
	pop[pop.index(get_worst(pop))] = copy.deepcopy(best)
	return pop

def make_inversion_sublist_mutation(pop):
	best = get_best(pop)
	for i in range(len(pop)):
		prob = np.random.randint(1,101)
		
		if(prob< CONST_MUTATION_RATE):
			c1,c2 = np.random.choice(range(0,CONST_CITIES),2, replace=False)
			aux = pop[i].cro[c1:c2]
			aux.reverse()
			pop[i].cro[c1:c2] = aux
	pop[pop.index(get_worst(pop))] = copy.deepcopy(best)
	return pop

def get_best(pop):
	return min(pop,key = lambda x:x.fit)

def get_worst(pop):
	return max(pop,key = lambda x:x.fit)

def plot_graph(city, m):
	x = []
	x1 = []
	y1 = []
	y = []
	for i in range(len(CITIES)):
		x.append(city[i][0])
		y.append(city[i][1])
		x1.append(CITIES[i][0])
		y1.append(CITIES[i][1])

	x.append(city[0][0])
	y.append(city[0][1])
	x1.append(CITIES[0][0])
	y1.append(CITIES[0][1])

	plt.figure()
	plt.plot(x,y,'x-')
	plt.plot(x1,y1,'x--', linewidth=0.5)


	plt.grid(color='grey', linestyle='-', linewidth=0.1)

	nameFigure = 'figure2_'+str(CONST_CROSS_RATE)+'_'+str(CONST_MUTATION_RATE) +'_'+str(CONST_POPULATION_SIZE)+'_'+str(CONS_GENERATIONS)+'_'+str(m)+'.png'
	plt.savefig(nameFigure)
	# plt.show()

def open_file(file):
	# Open input file
	infile = open(file, 'r')

	# Read instance header
	name = infile.readline().strip().split()[1] # NAME
	fileType = infile.readline().strip().split()[1] # TYPE
	comment = infile.readline().strip().split()[1] # COMMENT
	dimension = infile.readline().strip().split()[2] # DIMENSION
	edgeWeightType = infile.readline().strip().split()[1] # EDGE_WEIGHT_TYPE
	infile.readline()

	# Read node list
	nodelist = []
	for i in range(int(dimension)):
			x,y = infile.readline().strip().split()[1:]
			nodelist.append([int(x), int(y)])

	# Close input file
	infile.close()

	global	CONST_CITIES 
	CONST_CITIES = int(dimension)
	global	CITIES
	CITIES = nodelist
	
'''
MAIN
'''

open_file('att48.tsp')


nameFile = 'file2_'+str(CONST_CROSS_RATE)+'_'+str(CONST_MUTATION_RATE) +'_'+str(CONST_POPULATION_SIZE)+'_'+str(CONS_GENERATIONS)+'.txt'
f = open(nameFile, 'w+')
for m in range(0,5):
	f.write('Exe: ' + str(m) + '\n')
	pop = [Cromossomo() for i in range(CONST_POPULATION_SIZE)]
	# # Mostra a populacao inicial
	print('--------------------------------')
	print('-------Populacao Inicial--------')
	show_population(pop)
	best = []
	for i in range(0,CONS_GENERATIONS):		
		pop = make_tournament(pop)
		# show_population(pop)
		pop = make_obx(pop)
		# # show_population(pop)
		pop = make_ord_element_mutation(pop)
		# pop = make_inversion_sublist_mutation(pop)
		# # show_population(pop)
		# # A cada 100 geracoes mostra o melhor cromossomo
		if(i % 100 == 0 ):
			best = min(pop,key = lambda x:x.fit)
			f.write('Generation: ' + str(i) + ' Best_Fit: '+str(best.fit)+'\n')
			print('Generation: ' + str(i) + ' Best_Fit: '+str(best.fit))

	plot_graph(best.cro,m)
	# Mostra a populacao final
	print('--------------------------------')
	print('-------Populacao Final----------')
	show_population(pop)
	f.write('\n\n\nBest: ')
	for k in range(len(best.cro)):
		f.write(str(best.cro[k]))
	f.write('\n\n\n')
f.close()