import numpy as np
from sys import argv
from random import sample

'''
Constants
'''
# Porcentagem de taxa de crosover
CONST_CROSS_RATE = 98 

# Porcentagem de taxa de mutacao
CONST_MUTATION_RATE = 8

# Quantidade de bits por gene(dimensao)
CONST_GENE_SIZE = 22 

# Quantidade de genes no cromossomo
CONST_CROMOSSOME_SIZE = 5 

# Quantidade de geracoes
CONST_GENERATIONS = 600

# Tamanho da populcao
CONST_POPULATION_SIZE = 500

# Valor minimo 
CONST_MIN = -5.12

# Valor Maximo
CONST_MAX = 5.12

# Define uma classe para o cromossomo
class Cromossomo:
		def __init__(self, cr=[]):
			# Verifica se a entrada e um cromossomo valido ou nao. 
			# Se for um vazio cria um novo cromossomo
			self.cro = self.make_initial_cro() if cr == [] else cr

			# Calcula o fitnes para o cromosso
			self.fit = self.calc_fitnes()

		# Funcao para criar um cromossomo aleatorio
		def make_initial_cro(self):			

			# Cria um cromosso aleatorio com a quantidade de genes especificada
			pop = (CONST_CROMOSSOME_SIZE,CONST_GENE_SIZE)

			# Retorna este cromossomo criado
			return np.random.randint(0,2, size=pop) 
		
		# Funcao para o calculo do fitnes
		def calc_fitnes(self):
			fit=0
			#concatena todos os genes em um unico para fazer a conta do fitnes
			for i in range(len(self.cro)): 
				# declare value vazio
				value = [] 

				# Concatena os genes
				value.extend(self.cro[i])
				
				# transforma em nparray para poder calcular o inteiro
				value =  np.asarray(value) 

				# transforma em inteiro
				value = value.dot(2**np.arange(value.size)[::-1]) 
				
				# Converte em real
				value = CONST_MIN+(CONST_MAX-CONST_MIN)*((value)/(2**CONST_GENE_SIZE-1))

				# Calcula o fitnes sum(X^2)
				value = (value)**2
				fit += value	
			# Retonra o fitnes calculado
			return fit # calcula o fitnes

# Funcao para mostrar a populacao
def show_population(pop):
	pop = sorted(pop,key = lambda x:x.fit)
	for i in range(len(pop)):
		print('Ind: '+str(i)+'\t\tCro: '+str(pop[i].cro)+'\tFit: '+str(pop[i].fit))

# Funcao para realizar o torneio de 2
def make_tournament(pop):
	# Definie uma populacao intermediaria vazia
	pop_middle = []

	# Intera sobre todos os elementos da populacao
	for i in range(len(pop)):

		# Sorteia dois cromossomos para o torneio
		c1,c2=np.random.choice((len(pop)),2,replace=False) 

		# Verifica qual o melhor individuo 
		cr_max = min([pop[c1],pop[c2]],key = lambda x:x.fit) 

		# Adiciona este individuo na populacao
		pop_middle.append(Cromossomo(cr = cr_max.cro))
	
	# Retorna a nova populacao
	return pop_middle

# Funcao para realizar o crossover
def make_crossover(pop):
	# Define uma populacao intermediaria vazia
	pop_middle = [] 	

	
	# O for só vai até metada da poulação pois a cada iteracao dois individuos sao escolhidos e dois novos filhos sao gerados
	for i in range(int(len(pop)/2)):
		
		# Seleciona dois individuos para realizar o crosover
		c1,c2 = np.random.choice((len(pop)),2, replace=False)
		
		# Sorteia a probabilidade de fazer o crosover
		prob = np.random.randint(0,101) 
	
		# Testa a probabilidade de fazer o crosover
		if(prob<=CONST_CROSS_RATE): 
			# Cria dois vetores auxiliares
			cr_c1 = []
			cr_c2 = []
	
			# For para fazer com que todos os genes sejam concatenados
			for i in range(CONST_CROMOSSOME_SIZE):
				cr_c1.extend(pop[c1].cro[i])
				cr_c2.extend(pop[c2].cro[i])

			# Seleciona os pontos para fazer o crossover
			cut = sample(range(1, (CONST_GENE_SIZE*CONST_GENE_SIZE)-1), CONST_GENE_SIZE+1)
			cut.append(0)
			cut.append(CONST_GENE_SIZE)
			cut.sort()
			
			# For para fazer o crosover entre os dois individuos
			for s in range(len(cut)-1):
				cut_start = cut[s]
				cut_end = cut[s+1]
				# Troca somente os indices pares para nao ocorrer uma troca completa
				if( s % 2 == 0):
					aux = cr_c1[cut_start:cut_end]
					cr_c1[cut_start:cut_end] = cr_c2[cut_start:cut_end]
					cr_c2[cut_start:cut_end] = aux
			
			# Retorna os vetores criados para forma de um cromossomo com n genes
			cr_cross1 = cr_c1
			cr_cross2 = cr_c2
			seq = cr_cross1
			size = int(len(cr_cross1)/CONST_CROMOSSOME_SIZE)
			cr_cross1 = [seq[i:i+size] for i  in range(0, len(seq), size)]
			
			# Retorna os vetores criados para forma de um cromossomo com n genes
			seq = cr_cross2
			cr_cross2 = [seq[i:i+size] for i  in range(0, len(seq), size)]
			
			# Cria dois novos individuos e adiciona-os na poulacao intermediaria 
			pop_middle.append(Cromossomo(cr = cr_cross1))
			pop_middle.append(Cromossomo(cr = cr_cross2))
		else:
			# Caso nao tenha caido na probabilidade de fazer o crossover somente adicione os dois individuos na populacao intermediaria
			pop_middle.append(Cromossomo(cr = pop[c1].cro))
			pop_middle.append(Cromossomo(cr = pop[c2].cro))
	return pop_middle

# Funcao para realizar a mutacao
def make_mutation(pop):

	# Inicia a populacao intermediaria com a populacao
	pop_middle = pop
	for i in range(len(pop_middle)):  

		# Sorteia probabilidade de fazer a mutacao
		prob = np.random.randint(0,101) 

		# Testa a probabilidade de fazer a mutacao
		if(prob<=CONST_MUTATION_RATE):		
			
			# Definie um vetor auxiliar
			cross = []

			# Concatena todos os genes no vetor auxiliar
			for j in range(CONST_CROMOSSOME_SIZE):
				cross.extend(pop_middle[i].cro[j])

			# verifica bit a bit do cromossomo uma nova change de 50% de fazer ou nao a mutacao
			for k in range(len(cross)):	

				# Calcula a nova probabilidade
				prob2 = np.random.randint(0,101)

				# Testa se passou nos 50%
				if(prob2<=50):

					# Se passou verifica se e 1 ou 0 para trocar; Se nao passou nao faz nada
					if(cross[k] == 1):
						cross[k] = 0
					else:
						cross[k] = 1

			# Retorna na forma de cromossomo com n genes
			size = int(len(cross)/CONST_CROMOSSOME_SIZE)
			seq = cross
			cross = [seq[i:i+size] for i  in range(0, len(seq), size)]

			# Modifica o individuo da populacoa intemediaria 
			pop_middle[i] = Cromossomo(cr=cross)
	return pop_middle

'''
MAIN
'''
# Sorteia a populacao inicial
pop = [Cromossomo() for i in range(CONST_POPULATION_SIZE)]

# Mostra a populacao inicial
print('--------------------------------')
print('-------Populacao Inicial--------')
show_population(pop)

for i in range(0,CONST_GENERATIONS):		
  pop = make_tournament(pop)
  pop = make_crossover(pop)
  pop = make_mutation(pop)

	# A cada 100 geracoes mostra o melhor cromossomo
  if(i % 100 == 0 ):
    best = min(pop,key = lambda x:x.fit)
    print('Generation: ' + str(i) + ' Best_Fit: '+str(best.fit))
    #show_population(pop)
    #print(best.fit)

# Mostra a populacao final
print('--------------------------------')
print('-------Populacao Final----------')
show_population(pop)
