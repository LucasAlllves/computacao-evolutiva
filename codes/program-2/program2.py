import numpy as np
from sys import argv
from random import sample

'''
Constants
'''
CONST_CROSS_RATE = 98
CONST_MUTATION_RATE = 1
CONST_GENE_SIZE = 22 # qtd bits to produce integer
CONST_CROMOSSOME_SIZE = 1 # qtd genes que tem no cromosso
CONST_GENERATIONS = 100
CONST_POPULATION_SIZE = 100
CONST_MIN = -1
CONST_MAX = 2

'''
CONST_CROSS_RATE = int(argv[1])
CONST_MUTATION_RATE = int(argv[2])
CONST_GENE_SIZE = int(argv[3])
CONST_CROMOSSOME_SIZE  = int(argv[4])
CONST_GENERATIONS = int(argv[5])
'''

class Cromossomo:
		def __init__(self, cr=[]):
			# verify if is null, if is null make randon
			self.cro = self.make_initial_cro() if cr == [] else cr
			self.fit = self.calc_fitnes()
		def make_initial_cro(self):			
			pop = (CONST_CROMOSSOME_SIZE,CONST_GENE_SIZE)
			return np.random.randint(0,2, size=pop) # sortea o aleatório
		def calc_fitnes(self):
			fit=0
			#concatena todos os genes em um unico para fazer a conta do fitnes
			for i in range(len(self.cro)): 
				value = [] # declare value vazio
				value.extend(self.cro[i])
				value =  np.asarray(value) # transforma em nparray para poder calcular o inteiro
				value = value.dot(2**np.arange(value.size)[::-1]) # transforma em inteiro
				#print(value)
				value = CONST_MIN+(CONST_MAX-CONST_MIN)*((value)/(2**CONST_GENE_SIZE-1))
				#print(value)
				fit += value
			return fit*(np.sin(10*np.pi*fit))+1 # calcula o fitnes

def show_population(pop):
	pop = sorted(pop,key = lambda x:x.fit, reverse=True)
	for i in range(len(pop)):
		print('Ind: '+str(i)+'\t\tCro: '+str(pop[i].cro)+'\tFit: '+str(pop[i].fit))

def make_tournament(pop):
	pop_middle = []
	for i in range(len(pop)):
		c1,c2=np.random.choice((len(pop)),2,replace=False) #select 2 ind to make tournament
		cr_max = max([pop[c1],pop[c2]],key = lambda x:x.fit) #verify what is the best
		pop_middle.append(Cromossomo(cr = cr_max.cro))
	return pop_middle

def make_crossover(pop):
	pop_middle = [] # set intermeditate population	

	for i in range(int(len(pop)/2)):
		#c1,c2=np.random.choice((0,len(pop)-1),2,replace=False) #get tow individual not igual
		c1,c2 = np.random.choice((len(pop)),2, replace=False)
		#c1,c2 = sample(range(0, len(pop)-1), 2) #sorteia dos individuos
		prob = np.random.randint(0,101) # rand prob to make cross over
	
		if(prob<=CONST_CROSS_RATE): # if prob < cros rate do the cross
			cr_c1 = []
			cr_c2 = []
	
			for i in range(CONST_CROMOSSOME_SIZE):
				cr_c1.extend(pop[c1].cro[i])
				cr_c2.extend(pop[c2].cro[i])

			#cut_start = np.random.randint(1,4) #select one point to cut off
			cut = sample(range(1, CONST_GENE_SIZE-1), 8)
			cut.append(0)
			cut.append(CONST_GENE_SIZE)
			cut.sort()
			#print(cut)
			#print(cr_c1,cr_c2)
			for s in range(len(cut)-1):
				cut_start = cut[s]
				cut_end = cut[s+1]
				if( s % 2 == 0):
					aux = cr_c1[cut_start:cut_end]
					cr_c1[cut_start:cut_end] = cr_c2[cut_start:cut_end]
					cr_c2[cut_start:cut_end] = aux
			#print(cr_c1,cr_c2)					
			cr_cross1 = cr_c1
			cr_cross2 = cr_c2
			seq = cr_cross1
			size = int(len(cr_cross1)/CONST_CROMOSSOME_SIZE)
			cr_cross1 = [seq[i:i+size] for i  in range(0, len(seq), size)]
			
			seq = cr_cross2
			cr_cross2 = [seq[i:i+size] for i  in range(0, len(seq), size)]
			
			pop_middle.append(Cromossomo(cr = cr_cross1))
			pop_middle.append(Cromossomo(cr = cr_cross2))
		else:
			pop_middle.append(Cromossomo(cr = pop[c1].cro))
			pop_middle.append(Cromossomo(cr = pop[c2].cro))
	return pop_middle


def make_mutation(pop):
	pop_middle = pop
	for i in range(len(pop_middle)):
		cross = []
		for j in range(CONST_CROMOSSOME_SIZE):
			cross.extend(pop_middle[i].cro[j])
			#print(cross)
			for k in range(len(cross)):	
				prob = np.random.randint(0,101) # rand prob to make cross over
				if(prob<=CONST_MUTATION_RATE):	
				#print(prob,CONST_MUTATION_RATE)
				#print(cross[k])
					if(cross[k] == 1):
						cross[k] = 0
					else:
						cross[k] = 1
				#print(cross[k])
			#print(cross)
			size = int(len(cross)/CONST_CROMOSSOME_SIZE)
			seq = cross
			cross = [seq[i:i+size] for i  in range(0, len(seq), size)]
			pop_middle[i] = Cromossomo(cr = cross)
	return pop_middle

'''
MAIN
'''
#populate
pop = [Cromossomo() for i in range(CONST_POPULATION_SIZE)]
show_population(pop)
for i in range(0,CONST_GENERATIONS):		
	pop = make_tournament(pop)
	#print('tournament')
	#show_population(pop)
	#print('cross')
	pop = make_crossover(pop)
	#show_population(pop)
	#print('mut')
	pop = make_mutation(pop)
show_population(pop)
