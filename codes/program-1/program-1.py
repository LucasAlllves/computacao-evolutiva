# genetic  algorithm to find max x^2 [0,31]
# imports
import numpy as np
from sys import argv

'''

Constants      

'''

'''
CONST_CROSS_RATE = 95
CONST_MUTATION_RATE = 2
CONST_GENERATIONS = 600
'''
CONST_CROSS_RATE = int(argv[1])
CONST_MUTATION_RATE = int(argv[2])
CONST_POPULATION_SIZE = int(argv[3])
CONST_GENERATIONS = int(argv[4])


class Cromossomo:
  def __init__(self, cr=[]):
    self.cro = self.make_initial_cro() if cr == [] else cr # verify if is null, if is null make randon
    self.fit = self.calc_fitnes() 
  def make_initial_cro(self) :
    return np.random.randint(0,32)
  def calc_fitnes(self):
    return self.cro**2

def make_tournament(pop):
  pop_middle = []
  for i in range(len(pop)):
    c1,c2=np.random.choice((len(pop)-1),2,replace=False) #select 2 ind to make tournament
    cr_max = max([pop[c1],pop[c2]],key = lambda x:x.fit) #verify what is the best
    pop_middle.append(Cromossomo(cr = cr_max.cro))
    #pop_middle.append(pop[c1]) if pop[c1].fit > pop[c2].fit else pop_middle.append(pop[c2])
  return pop_middle

def make_crossover(pop):
  cut_start = np.random.randint(1,4) #select one point to cut off
  cut_end = cut_start - 5 
  pop_middle = pop # set intermeditate population
  for i in range(len(pop)): 
    c1,c2=np.random.choice((0,len(pop)-1),2,replace=False) #get tow individual not igual
    prob = np.random.randint(0,101) # rand prob to make cross over
    if(prob<=CONST_CROSS_RATE): # if prob < cros rate do the cross
      cr_cross1 = int(str('{:05b}'.format(pop[c1].cro))[:cut_start]+str('{:05b}'.format(pop[c2].cro))[cut_end:],2) #make the first child
      pop_middle[c1] = Cromossomo(cr = cr_cross1)
      cr_cross2 = int(str('{:05b}'.format(pop[c2].cro))[:cut_start]+str('{:05b}'.format(pop[c1].cro))[cut_end:],2) #make the second child
      pop_middle[c2] = Cromossomo(cr = cr_cross2)
  pop = pop_middle
  return pop
      
def make_mutation(pop):
  pop_middle = pop # set middle population
  for i in range(len(pop_middle)): # for each member of pop
    individual = intToBinString(pop[i].cro)
    print(individual)
    for j in range(len(individual)):
      mutation_prob = np.random.randint(0,101) # draft ramdom probability to make mutation on this letter     
      if(mutation_prob<=CONST_MUTATION_RATE):
        if(individual[j] == '1'):
          individual[j] == '0'
        else:
          individual[j] == '1'
    pop_middle[i] = Cromossomo(cr = int(individual,2))    
  return pop_middle

def intToBinString(value):
    return str('{:05b}'.format(value))

def show_population(pop):
  for i in range(len(pop)):
    print('Ind: '+str(i)+'\t\tCro: '+str(pop[i].cro)+'\t\tBin:'+intToBinString(pop[i].cro)+'\tFit: '+str(pop[i].fit))

  
#populate
pop = [Cromossomo() for i in range(CONST_POPULATION_SIZE)]
print('\n---------------- Populacao inicial ----------------')
show_population(pop)
for i in range(0, CONST_GENERATIONS):
  #print('\n---------------- Geracao: ',i,'----------------')
  #print('\n---------------- Tournament -------------------\n')
  pop = make_tournament(pop)
  #show_population(pop)
  #print('\n---------------- Crossover -------------------\n')
  pop = make_crossover(pop)
  #show_population(pop)
  #print('\n---------------- Mutation -------------------\n')
  pop = make_mutation(pop)
  #show_population(pop)
show_population(pop)
